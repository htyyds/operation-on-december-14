import java.util.Scanner;
public class Alice {

	public static void main(String[] args) {
		// 一家贸易公司有4位销售员，每位销售员负责销售相同的4种商品
//		使用二维数组，编写一个程序，接收每名销售员销售的各种产品的数量。 打印产品销售明细表，
//		明细表包括：每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比
		Scanner ht = new Scanner(System.in);
		double sum=0,xiao=0;
		double[] [] many=new double[4] [4];
		for (int i = 0; i < many.length; i++) {
			System.out.println("请输入第"+(i+1)+"位销售员的A种销售产品量");
			many[i] [0]= ht.nextDouble();
			System.out.println("请输入第"+(i+1)+"位销售员的B销售产品量");
			many[i] [1]= ht.nextDouble();
			System.out.println("请输入第"+(i+1)+"位销售员的C种产品量");
			many[i] [2]= ht.nextDouble();
			System.out.println("请输入第"+(i+1)+"位销售员的D种产品销售量");
			many[i] [3]= ht.nextDouble();	
			sum=(many[i][0]+many[i][1]+many[i][2]+many[i][3]);
			xiao=many[3][i];
		}
//		销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）∗100
//		总销售量指各类产品销售量的总和
		System.out.println("----12月14号销售员的销售情况----");
		System.out.println("商品\t 销售1\t 销售2\t 销售3\t 销售4\t 销售总数\t 销售占比");
		for (int i = 0; i < many.length; i++) {
			System.out.print("商品"+i+"\t");
			for(int j=1;j<4;j++) {
				sum+=many[i][j];
			}
			System.out.print(many[i][0]+"\t"+many[i][1]+"\t"+many[i][2]+"\t"+many[i][3]+"\t");
			System.out.print("  "+(many[i][0]+many[i][1]+many[i][2]+many[i][3]));
			System.out.print("    "+(xiao/sum)*100+"%");
			System.out.println();
		}

	}

}
